#*************************************************#
# ------ auto-py-to-exe et convertico ------- #
#*************************************************#
"""
- pip install auto-py-to-exe -> Installer le fichier de transformation en executable
- https://convertico.com -> Convertir une image en fichier icon
"""
#*************************************************#
# -------- Importation des modules --------- #
#*************************************************#
import tkinter
from math import *
#*************************************************#
# ----------- Fonctionnalités ------------ #
#*************************************************#

#----- Sauvegarde ce que nous avons comme expression dans la calculatrice
equation = ""

#----- Recuper la valeur d'un bouton
def appuyer(bouton):
    if bouton == "=":
        calculer()
        return
    global equation
    if bouton == "!":
        equation = f"factorial({equation})"
        expression.set(equation)
    elif bouton == "^":
        equation += "**"
        expression.set(equation)
    else:
        equation += str(bouton)
        expression.set(equation)

#----- Effectuer le calculer
def calculer():
    try:
        global equation
        resultat = str(round(eval(equation), 15))
        expression.set(resultat)
        equation = resultat
    except ZeroDivisionError and ValueError:
        expression.set("Math Error")
        equation = ""
    except:
        expression.set("Syntax Error")
        equation = ""

#----- Effacer l'ecran
def effacer():
    global equation
    equation = ""
    expression.set("")

#*************************************************#
# -------------- Calculatrice -------------- #
#*************************************************#
app = tkinter.Tk()

#----- Couleurs
Blanc = "#FFFFFC"
Noir = "#101410"
Blue = "#87CEEB"
Rouge = "#F94447"

#----- Couleur de fond
app.configure(background = Noir)

#----- Titre de l'application
app.title("Calculatrice")

#----- Taille et positionnement de la fenêtre
app.resizable(width = False, height = False) #Redimensionnement
screenX = int(app.winfo_screenwidth()) #Largeur de l'ecran
screenY = int(app.winfo_screenheight()) #Hauteur de l'ecran
windowsX = 312
windowsY = 601
posX = (screenX // 2) - (windowsX // 2) #Center en largeur
posY = (screenY // 2) - (windowsY // 2) #Centre en hauteur
geometry = "{}x{}+{}+{}".format(windowsX, windowsY, posX, posY)
app.geometry(geometry)

#----- Variable pour stocker le contenu actuelle
expression = tkinter.StringVar()

#----- Boite de resultat
resultat = tkinter.Label(app, bg = Noir, fg = Blanc, textvariable = expression, height =
3, font = "Bahnschrift 15 bold")
resultat.grid(columnspan = 4)

#----- Boutons
boutons = ["^", "sin", "cos","tan", "exp", "log", "log10", "sqrt", "!", "(", ")", "%", 7,
8, 9, "*", 4, 5, 6, "/", 1, 2, 3, "+", 0, ".", "=", "-"]
ligne = 1
colonne = 0
for b in boutons:
    bouton = tkinter.Label(app, text = str(b), bg = Blue, fg = Noir, height = 3, width =
    8, font = "Bahnschrift 12 bold")
    #----- Afficher le texte cliquable
    bouton.bind("<Button-1>", lambda e, b = b: appuyer(b))
    bouton.grid(row = ligne, column = colonne)
    colonne += 1
    if colonne == 4:
        colonne = 0
        ligne += 1

bouton = tkinter.Label(app, text = "Effacer", bg = Rouge, fg = Blanc, height = 4, width =
34, font = "Bahnschrift 12 bold")
bouton.bind("<Button-1>", lambda e: effacer())
bouton.grid(columnspan = 4)

app.mainloop()